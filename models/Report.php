<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Report
 *
 * @author dtecno
 */
class Report extends \Fox\FoxModel {
    //put your code here
    
    private $id;
    private $title;
    private $location;
    private $user;
    private $date;
    private $time;
    
    function __construct($id, $title, $location, $user, $date, $time) {
        parent::__construct();
        $this->id = $id;
        $this->title = $title;
        $this->location = $location;
        $this->user = $user;
        $this->date = $date;
        $this->time = $time;
    }

    function getId() {
        return $this->id;
    }

    function getTitle() {
        return $this->title;
    }

    function getLocation() {
        return $this->location;
    }

    function getUser() {
        return $this->user;
    }

    function getDate() {
        return $this->date;
    }

    function getTime() {
        return $this->time;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function setLocation($location) {
        $this->location = $location;
    }

    function setUser($user) {
        $this->user = $user;
    }

    function setDate($date) {
        $this->date = $date;
    }

    function setTime($time) {
        $this->time = $time;
    }

        
    public function getMyVars(){
        return get_object_vars($this);
    }
}
