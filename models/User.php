<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Model
 *
 * @author Pabhoz
 */
class User extends \Fox\FoxModel {

  private $id;
  private $username;
  private $password;
  private $email;
  private $age;
  private $sex;
  private $phone;

  function __construct($id,$username,$password,$email,$age,$sex,$phone) {
        parent::__construct();
       $this->id = $id;
       $this->username = $username;
       $this->password = $password;
       $this->email = $email;
       $this->age = $age;
       $this->sex = $sex;
       $this->phone = $phone;
  }

    public function getId(){
        return $this->$id;
    }

    public function setId($id){
        $this->id = $id;
    }

    public function getUsername(){
        return $this->username;
    }

    public function setUsername($username){
        $this->username = $username;
    }

    public function getPassword(){
        return $this->password;
    }

    public function setPassword($password){
        $this->password = $password;
    }

    public function getEmail(){
        return $this->email;
    }

    public function setEmail($email){
        $this->email = $email;
    }

    public function getAge(){
        return $this->age;
    }

    public function setAge($age){
        $this->age = $age;
    }

    public function getSex(){
        return $this->sex;
    }

    public function setSex($sex){
        $this->sex = $sex;
    }

    public function getPhone(){
        return $this->phone;
    }

    public function setPhone($phone){
        $this->phone = $phone;
    }

    public function getMyVars(){
        return get_object_vars($this);
    }

}
